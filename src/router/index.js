import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import About from '@/components/About'
import Productos from '@/components/Productos'
import Novedades from '@/components/Novedades'
import FAQ from '@/components/FAQ'
import Contacto from '@/components/Contacto'
import Proceso from '@/components/Proceso'
import Preinscripcion from '@/components/Preinscripcion'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/acerca-de',
      name: 'About',
      component: About
    },
    {
      path: '/productos',
      name: 'Productos',
      component: Productos
    },
    {
      path: '/novedades',
      name: 'Novedades',
      component: Novedades
    },
    {
      path: '/FAQ',
      name: 'FAQ',
      component: FAQ
    },
    {
      path: '/contacto',
      name: 'Contacto',
      component: Contacto
    },
    {
      path: '/proceso',
      name: 'Proceso',
      component: Proceso
    },
    {
      path: '/pre-inscripcion',
      name: 'Preinscripcion',
      component: Preinscripcion
    },
    {
      path: '/pre-inscripcion/:id',
      name: 'Preinscripcion',
      component: Preinscripcion
    }
  ]
})
